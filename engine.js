function qS(selector) { return document.querySelector(selector) }

window.g = {}

function resetState() {
	g = {
		'loopDuration': 100,
		'turnCounter': 0,
		'blue': {
			'element': qS('#blue'),
			'sprite': qS('#blue .sprite'),
			'position': 4,
			'jumpState': 0,
			'jumpHeight': 0,
			'ducking': false,
			'inertia': null,
			'facing': '-1',
			'keypresses': {
				'right': false,
				'left': false,
				'top': false,
				'down': false
			},
		},
		'red': {
			'element': qS('#red'),
			'sprite': qS('#red .sprite'),
			'position': 92,
			'jumpState': 0,
			'jumpHeight': 0,
			'ducking': false,
			'inertia': null,
			'facing': '1',
			'keypresses': {
				'right': false,
				'left': false,
				'top': false,
				'down': false
			},
		},
		'turnDuration': 10000,
		'kisser': null,
		'kisserTimer': 0,
		'kissSound': new Audio('kiss.mp3'),
		'hitcount': 0,
		'hitguard': 0,
		'winner': null,
		'keyMap': {
			'ArrowRight':	['red', 'right' ],
			'ArrowLeft':	['red', 'left' ],
			'ArrowUp':		['red', 'top' ],
			'ArrowDown':	['red', 'down' ],
			'd':			['blue', 'right' ],
			'q':			['blue', 'left' ],
			'z':			['blue', 'top' ],
			's':			['blue', 'down' ]
		},
		'jumpStates': {
			1: 16,
			2: 24,
			3: 28,
			4: 24,
			5: 16,
			6: 0
		},
		'resultsHaveBeenDisplayed': false,
		'turnScores': [0, 0, 0, 0, 0, 0]
	}
}

resetState()


document.onkeydown = keypress => processKeyDown(keypress.key)
document.onkeyup = keypress => processKeyUp(keypress.key)

function processKeyDown(key) {
	try {
		var player = g.keyMap[key][0]
		var direction = g.keyMap[key][1]
		g[player].keypresses[direction] = true
	} catch (error) {
		// an unmapped key was pressed, so we do nothing
	}

}

function processKeyUp(key) {
	try {
		var player = g.keyMap[key][0]
		var direction = g.keyMap[key][1]
		g[player].keypresses[direction] = false
	} catch (error) {
		// an unmapped key was pressed, so we do nothing
	}
}


setInterval(loop, g.loopDuration)

function loop() {

	var players = {
		'red': {'left': false, 'right': false, 'top': false, 'down': false, 'gamepad': null},
		'blue': {'left': false, 'right': false, 'top': false, 'down': false, 'gamepad': null}
	}

	// Gamepad inputs
	if (navigator.getGamepads().length == 2) {
		// 2 gamepads have indeed been detected !
		players['red'].gamepad = navigator.getGamepads()[0]
		players['blue'].gamepad = navigator.getGamepads()[1]
		for (var player in players) {
			gamepad = player.gamepad
			if (gamepad.axes[0] == -1) {
				player['left'] = true
			} else if (gamepad.axes[0] == 1) {
				player['right'] = true
			}
			if (gamepad.axes[1] == -1) {
				player['top'] = true
			} else if (gamepad.axes[1] == 1) {
				player['down'] = true
			}
		}
	}

	// Keyboard inputs
	for (var player in players) {
		players[player]['left'] = g[player].keypresses['left']
		players[player]['right'] = g[player].keypresses['right']
		players[player]['top'] = g[player].keypresses['top']
		players[player]['down'] = g[player].keypresses['down']
	}

	for (var player in players) {

		var instr = players[player]

		if (instr['left']) {
			moveLeft(player)
			g[player].inertia = 'left'
		} else if (instr['right']) {
			moveRight(player)
			g[player].inertia = 'right'
		} else if (g[player].inertia == 'left') {
			g[player].inertia = null
			moveLeft(player)
		} else if (g[player].inertia == 'right') {
			g[player].inertia = null
			moveRight(player)
		}

		if (instr['top'] && g[player].jumpState == 0) {
			// we can only jump if we're not jumping already !
			g[player].jumpState = 1
		} else if (g[player].jumpState == 7) {
			// reset at end
			g[player].jumpState = 0
		} else if (g[player].jumpState > 0) {
			g[player].jumpHeight = g.jumpStates[g[player].jumpState]
			g[player].jumpState += 1
		}

//		if (instr['down']) {
//			g[player].ducking = true
//		} else {
//			g[player].ducking = false
//		}

	}


	if (g.turnCounter == 0) {
		// the game has not started yet
		qS('#disclaimer').style.display == 'block'
		if (g['red'].inertia && g['blue'].inertia ) {
			// players are moving ! begin the game !
			g.turnCounter = 1
			qS('#disclaimer').style.display = 'none'
		}

	} else {

		var turnIsOver = g.kisserTimer / 1000 <= 0

		if (turnIsOver) {
			g.turnScores[g.turnCounter-2] = g.hitcount
			g.turnCounter += 1
			if (g.turnCounter > 0 && g.turnCounter < 8) { triggerAudio(g.turnCounter-2) }
		}

		if (g.turnCounter == 8) {
			// the game is over, display results !
			g.kisserTimer -= g.loopDuration // make it last a full turn
			if (!g.resultsHaveBeenDisplayed) {
				g.kisserTimer = g.turnDuration
				g.resultsHaveBeenDisplayed = true
				displayResults()				
				qS('#frame').classList.remove('kiss') // in case "smack" is still showing
			}
		} else if (g.turnCounter == 9) {
			// results have been displayed for 10 seconds
			// we can move back to default state
			resetState()
			qS('#disclaimer').style.display = 'block'
			qS('#scoreboard').style.display = 'none'
		} else if (g.turnCounter > 0 && g.turnCounter < 8) {
			// the game is under way

			if (turnIsOver) {
				// the turn is over, collect results and switch players
				g.kisserTimer = g.turnDuration
				if (g.kisser == 'blue') {
					g.kisser = 'red'
					qS('#blue').classList.remove('kisser')
					qS('#red').classList.add('kisser')
				} else {
					g.kisser = 'blue'
					qS('#red').classList.remove('kisser')
					qS('#blue').classList.add('kisser')
				}
				g.hitcount = 0				
			} else {
				// the turn is under way
				g.kisserTimer -= g.loopDuration
				// listen for hits
				var posDiff = g.blue.position - g.red.position
				var jumpDiff = g.blue.jumpHeight - g.red.jumpHeight
				if (g.hitguard == 0) {
					// hitguard activates when a hit occurs, so that no more hits are registered
					// if hitguard is inactive, we can check for new hits
					if (posDiff >= -6 && posDiff <= 6 ) {
						if (jumpDiff >= -12 && jumpDiff <= 12) {
							// both players are close enough, so a hit is registered
							g.hitcount += 1
							qS('#frame').classList.add('kiss')
							g.kissSound.play()
							// and hitguard triggers, protecting from future hits for a few loops
							g.hitguard = 8
						}
					}		
				} else {
					// if hitguard is active, we decrease its counter for next turn
					if (g.hitguard == 1) {
						qS('#frame').classList.remove('kiss')
					}
					g.hitguard -= 1
				}
			}
		}
	}
	render()
}

function render() {

	for (player of ['blue', 'red'] ) {

		var pos = g[player].position
		var jump = g[player].jumpHeight
		var facing = g[player].facing
		if (g[player].ducking) { var scale = .4 }
		else { var scale = 1 }
		g[player].element.style.transform = `translate(${pos}vw, -${jump}vw) scaleX(${facing})`
		g[player].sprite.style.transform = `scaley(${scale})`

	}
	if (qS('#hitcount').innerText != g.hitcount) {
		if (g.hitcount == 0) {
			qS('#hitcount').innerText = ""
		} else {
			qS('#hitcount').innerText = g.hitcount			
		}
	} 

}

window.audioMap = [
	'tragedie1.mp3',
	'frozen1.mp3',
	'tragedie2.mp3',
	'frozen2.mp3',
	'tragedie3.mp3',
	'frozen3.mp3',
	''
]

window.currentMusic = null

function triggerAudio(turn) {
	if (currentMusic) { currentMusic.pause() }
	currentMusic = new Audio(audioMap[turn])
	currentMusic.play()
}

function moveRight(player) {
	if (g[player].inertia == 'right') {
		g[player].position += 6
	} else {
		g[player].position += 3
	}
	// but you can't move past the wall !
	if (g[player].position > 92) {
		g[player].position = 92
		// 100 - 8 for the player width
	}
	g[player].facing = '-1'
}

function moveLeft(player) {
	if (g[player].inertia == 'left') {
		g[player].position -= 6
	} else {
		g[player].position -= 3
	}
	// but you can't move past the wall !
	if (g[player].position < 4) {
		g[player].position = 4
	}
	g[player].facing = '1'
}

function displayResults() {
	var blueScore = g.turnScores[0] + g.turnScores[2] + g.turnScores[4]
	var redScore = g.turnScores[1] + g.turnScores[3] + g.turnScores[5]
	if (blueScore > redScore) {
		g.winner = 'blue'
		qS('#score-conclusion').innerText = "Pierre a gagné la bataille des bisous !"
		triggerAudio(0)
	} else if (redScore > blueScore) {
		g.winner = 'red'
		qS('#score-conclusion').innerText = "Marie a gagné la bataille des bisous !"		
		triggerAudio(5)
	} else {
		g.winner = 'tie'
		qS('#score-conclusion').innerText = "C'est une égalité ! Ex aequo !"
	}

	qS('#score-details').innerText = `Pierre ${blueScore} - Marie ${redScore}`
	qS('#scoreboard').style.display = 'block'
}